CONFIGURING THE PHP.INI IS NECESSARY AND U SHOULD RESTART THE SERVER AGAIN AND THEN RUN THE CODE

mongo-db: it is no sql databse which stores data in the format of json i.e the relational database stores the data in the 

format of tables within rows, where foreign key is used for refering to different tables to access data.

mongo-db recommended for (distributed, unstructured)

as data needs to be distributed, relational database performs data sharding (chop off data and store on other server), 

mongo-db manages all this by itself
mongo uses indexing same as realtional db
mongo uses centralised server for managing data that is distributed and this centralised server is relational db as 

searching method is faster.

variables in php are case sensitive (defined by user)
functions and keywords are case insensitive (predefined by PHP)

typecasting means to convert value of variable 
type juggling means changing the datatype of the variable

variable expanding "" -> works as Javascript where `` is used
		   '' -> cannot be used with variables

arrays are pass by value by default in php (&) can be used to pass them by reference

array cannot be echoed instead print_r() needs to be used

arrays supported in PHP are linear arrays and associative arrays (can also be mixed)

arrays are immutable in PHP


$var = 10;
$string = <<<VALUE
	//HTML
VALUE;

echo $string;

echo <<<HEREDOC 
	//HTML
HEREDOC;

the above piece of code is called HEREDOC which is used to print huge data or to use variables of PHP within HTML enclosed 

in PHP

variables must be used as {$var}
<ul>
	<li>{$var}</li>
</ul>

2 imp things in sen: development and production, in most of the cases the code fails in production phase

die: used to terminate the script, it does not execute any code following it hence is not user friendly

'@' is a prefix which can be used for suppressing errors

constants are defined in php using define() function
Syntax: define("VARIABLE_NAME", "VARIABLE_VALUE")
Example: define("HOST", "localhost")
printed using echo alongwith constant(VARIABLE_NAME) 
Note: above way is not recommended

/**********BETTER WAY**************/
Use INI file (initialization)
Create a file with extension with ".ini" (eg: config.ini)
you can add comment within []
add [database]
key = value
eg: host = localhost (without comma seperated)
    dbname = address_book

NOTE: Add this file outside the root folder as this will avoid breach of data via linux server as the server is not 

allowed to access the file outside the folders (can access only url files)

Access these constants using parse_ini_file
eg: $const = parse_ini_file("config.ini") //returns array, can be used with help of key 
    $const['host']

Consider: A case where database is to be created for 'Student Enrolls For Course'; the tables to be designed for this 

statement will be Course, Student & Enrollment ... 
Course table consists of the courses 
Student table consists of the student data
Whereas, Enrollment will act as a mediator which will be the link between Course & Student where the foreign key will 

refer to the primary keys of Student & Course ID, one method of retrieving the primary key from Student table is to get 

the maxID in the table, but it might happen that while retrieving maxID 10-15 more records are inserted (as millions of 

records get inserted in 1ms on server), for this purpose PHP provides a method 'mysqli_insert_id(conn)' which returns the 

last id of the last record that was inserted.
//insertId

when including a file in root folder it's scope is for that root folder, all the files at that level can use that file.

SUPER GLOBAL ARRAYS provided by PHP
$_GET[]
$_POST[]
$_SERVER[]
$_FILES[] etc...
query-strings used to store status from one page to another
http://www.somewebsite.com/?var1=value1&var2=value2           //?var1=value1&var2=value2 => query string

***
enctype = "multipart/form-data"
it is very important to set enctype as any file rather then rawtext might have to be submitted by a form, a small file can 

be handled but a file consisting of images etc needs it's enctype to be set.

datepicker is provided by materialize but not invoked/called automatically, we have to do that manually
it provides date which needs to be converted into date format required by the database

$_SERVER[] : It stores the info related to the server
$_FILES[] : It stores the additional files i.e. the files rather then the data, it stores the these files in the 

additional payload.

explode(char, value) method splits the given value by the passed character and returns an array
end(array) method returns the last index value from the array passed to as an argument
header() method allows you to set the HTTP attributes
attributes: location (case-insensitive)
toastr notification : used to notify the user


<!--
    <script>
    <?php
    $q = '';
    $op = '';
    if(isset($_GET['q'])){
        $q = $_GET['q'];
    }
    
    if(isset($_GET['op'])){
        $op = $_GET['op'];
    }  

    if($q==='success' && $op =='insert'):
    ?>
        let toastHTML = "<span>New Contact Created Successfully!</span>";
        let classList = "green darken-1";
    <?php
    endif;    
    ?>
    M.toast({
        html: toastHTML,
        classes: classList
    });     
    </script>
-->