const http = new slHTTP();
const put = document.getElementById('table');

http.get('http://localhost:8080/test.php')
    .then(data => {
//    var data1 = JSON.parse(data);
    console.log(data);
    showData(data);
})    
    .catch(err => console.log(err));

//<tr>
//                        <td><img class="circle" src="images/users/pedro-chapman.jpg" alt="" height="60%"></td>
//                        <td>Pedro Chapman</td>
//                        <td>pedro.chapman@example.com</td>
//                        <td>1/4/1952</td>
//                        <td>(107)-138-3450</td>
//                        <td>5750 Poplar Dr</td>
//                        <td><a class="btn btn-floating green lighten-2"><i class="material-icons">edit</i></a></td>
//                        <td><a class="btn btn-floating red lighten-2 modal-trigger" href="#deleteModal"><i class="material-icons">delete_forever</i></a>
//                        </td>
//                    </tr>
function showData(user){
    output = "";
    for(i=0; i<user.length ;i++)
     {
        output += `
        <tr>
            <td><img class="circle" src="images/users/${user[i].image_name}" alt="" height="60%"></td>
            <td>${user[i].first_name} ${user[i].last_name}</td>
            <td>${user[i].email}</td>
            <td>${user[i].birthdate}</td>
            <td>${user[i].telephone}</td>
            <td>${user[i].address}</td>
            <td><a class="btn btn-floating green lighten-2"><i class="material-icons">edit</i></a></td>
            <td><a class="btn btn-floating red lighten-2 modal-trigger" href="#deleteModal"><i class="material-icons">delete_forever</i></a>
            </td>
        </tr>
`;
    }
    put.innerHTML = output;
}